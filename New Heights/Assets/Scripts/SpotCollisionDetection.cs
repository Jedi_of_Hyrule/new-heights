using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpotCollisionDetection : MonoBehaviour
{
    public static int spotContactCounter = 0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Entered");
        spotContactCounter++;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Exited");
        spotContactCounter--;
    }
}