using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MeterTracker : MonoBehaviour
{
    public TextMeshProUGUI meters;
    public float distance = 0f;
    [SerializeField] Canvas canvas;
    float count;
    [SerializeField] public BackgroundMovement BackgroundMovement;

    private void Start()
    {
        meters.text = distance + " m";
    }

    // Update is called once per frame
    void Update()
    {
        if (canvas.isActiveAndEnabled == false && BackgroundMovement.backgroundSpeed > 0)
        {
            meters.text = Mathf.Round(distance) + " m";
            distance += (BackgroundMovement.backgroundSpeed / 10) * Time.deltaTime;
        }
    }
}
