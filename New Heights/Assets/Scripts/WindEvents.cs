using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WindEvents : MonoBehaviour
{

    public int eventTime;
    public float boostSpeed = 5;

    bool QTEOccuring;
    int QTETimer = 1;

    [SerializeField] GameObject spotsPrefab;
    private GameObject[] spots;
    private bool[] spotsTouched;

    [SerializeField] public BackgroundMovement BackgroundMovement;

    [SerializeField] MeterTracker counter;
    [SerializeField] Canvas success;
    [SerializeField] Canvas fail;

    // event that triggers event after a random n of seconds
    public void Start()
    {
        StartCoroutine(EventTimer()); // starts event timer coroutine
        success.enabled = false;
        fail.enabled = false;
    }


    public void QuickTimeEventManager()
    {
        StartCoroutine(QTETime()); // begins the timer for the coroutine

        if (QTEOccuring == true)
        {
            CreateSpots();
        }
    }

    public void CreateSpots()
    {
        SpotCollisionDetection.spotContactCounter = 0;

        GameObject prefabInstance = Instantiate(spotsPrefab, GetRandomPosition(), Quaternion.identity); // Instantiate the prefab

        spots = new GameObject[4];
        spotsTouched = new bool[4];
        for (int i = 0; i < prefabInstance.transform.childCount; i++)
        {
            spots[i] = prefabInstance.transform.GetChild(i).gameObject;
        }

    }

    Vector3 GetRandomPosition()
    {
        float x = Random.Range(-6f, 6f);
        float y = Random.Range(-2f, 2f);
        return new Vector3(x, y, 0f);
    }


    // timers

    IEnumerator EventTimer() // handle when a QTE event can occur
    {
        Debug.Log("Event Timer called");
        eventTime = Random.Range(4, 8); // chooses a random number to begin the timer
        yield return new WaitForSeconds(eventTime); // takes the random number to use for seconds

        QuickTimeEventManager(); // after seconds, creates the event
        Debug.Log("Event Occured");

    }

    IEnumerator QTETime()
    {
        bool QTESuccess = false;
        QTEOccuring = true;
        float endTime = Time.time + QTETimer;

        while (endTime > Time.time)
        {
            yield return new WaitForEndOfFrame();
            if (SpotCollisionDetection.spotContactCounter == spots.Length)
            {
                QTESuccess = true;
                break;
            }
        }
        for (int i = 0; i < spots.Length; i++)
        {
            Destroy(spots[i]);
        }

        if(QTESuccess == true)
        {
            counter.distance += 10;
            BackgroundMovement.backgroundSpeed += 50;
            StartCoroutine(ScoreDisplay(true));
        }
        else
        {
            counter.distance -= 10;
            BackgroundMovement.backgroundSpeed -= 50;
            StartCoroutine(ScoreDisplay(false));
        }


        QTEOccuring = false; // set the QTE event off

        StartCoroutine(EventTimer()); // reset the Event Timer
    }

    IEnumerator ScoreDisplay(bool qteEvent)
    {

        if (qteEvent == true)
        {
            success.enabled = true;
            Debug.Log("success");
        }
        if (qteEvent == false)
        {
            fail.enabled = true;
            Debug.Log("fail");
        }
        yield return new WaitForSeconds(1);
        fail.enabled = false;
        success.enabled = false;

    }

}


