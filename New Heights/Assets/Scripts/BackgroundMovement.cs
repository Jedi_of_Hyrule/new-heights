using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour
{

    public float backgroundSpeed;
    public bool playerAlive; // decides if background speed is > 0
    public float distanceLostPerSecond = 2f; // how the background speed decreases every second
    [SerializeField] private Canvas endGameCanvas;
    [SerializeField] SquirrelMovement player;
    [SerializeField] GameObject forest;
    [SerializeField] GameObject city;
    int rand;
    [SerializeField] StartCounter begin;

    public void Start()
    {
        playerAlive = true;
        backgroundSpeed = 100f; // sets the intitial background speed
        StartCoroutine(BackgroundDecreaseTimer()); // starts the background movement timer
        endGameCanvas.enabled = false; // disable the end game canvas initially
    }


    public void Update()
    {
        if (backgroundSpeed > 0) // if background is still 'moving'
        {
            playerAlive = true;
            if (begin.start == true)
            {
                //StartCoroutine(BGSwitch());
            }
        }
        else //if background is 'still'
        {
            Debug.Log("Player Lost");
            playerAlive = false;
            endGameCanvas.enabled = true; // disable the end game canvas initially
            //end game logic
            player.mainVelocity = 0;
            player.downvel = 0;
            player.leftvel = 0;
            player.rightvel = 0;
            player.upvel = 0;
        }

    }

    public void ChangeBackgroundSpeed() 
    {

        StartCoroutine(BackgroundDecreaseTimer());

        if (playerAlive == true)
        {
            backgroundSpeed = backgroundSpeed - distanceLostPerSecond;
        }

    }

    IEnumerator BackgroundDecreaseTimer()
    {
        yield return new WaitForSeconds(1);
        ChangeBackgroundSpeed();
    }

    void BackgroundMoves(GameObject curr)
    {
        curr.transform.localScale += new Vector3((backgroundSpeed / 100) * Time.deltaTime, (backgroundSpeed / 100) * Time.deltaTime, 0);
        curr.transform.Translate(Vector2.down * (backgroundSpeed) * Time.deltaTime);
    }
/*
    IEnumerator BGSwitch()
    {
        rand = Random.Range(1, 3);
        if (rand == 1)
        {
            GameObject scene = Instantiate(forest, new Vector3(0, 0, 0), Quaternion.identity);
            Debug.Log("forest");
            BackgroundMoves(scene);
            yield return new WaitForSeconds(2000 / backgroundSpeed);
            Destroy(scene);
        }
        if (rand == 2)
        {
            GameObject scene = Instantiate(city, new Vector3(0, 0, 0), Quaternion.identity);
            Debug.Log("city");
            BackgroundMoves(scene);
            yield return new WaitForSeconds(2000 / backgroundSpeed);
            Destroy(scene);
        }

        yield return new WaitForSeconds(2000 / backgroundSpeed);
    }*/
}
