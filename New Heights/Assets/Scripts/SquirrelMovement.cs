using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquirrelMovement : MonoBehaviour
{
    //squirrel body parts
    [SerializeField] GameObject body;
    [SerializeField] GameObject lleg;
    [SerializeField] GameObject rleg;
    [SerializeField] GameObject larm;
    [SerializeField] GameObject rarm;
    [SerializeField] Vector2 target;
    public float mainVelocity; //speed that the limb's move
    float llegvel, rlegvel, larmvel, rarmvel; //individual variables for controlling speed on each limb
    public float upvel, downvel, rightvel, leftvel; //speed that the body moves across the screen
    float llegpos, rlegpos, larmpos, rarmpos; //locations of each limb
    [SerializeField] Canvas canvas;
    void Start()
    {
        //tracks where limbs are to stop them moving too far
        llegpos = body.transform.position.y + 0.4f;
        rlegpos = body.transform.position.y + 0.4f;
        larmpos = body.transform.position.y - 0.4f;
        rarmpos = body.transform.position.y - 0.4f;
    }

    void Update()
    {
        //updates limb positions based on new body position
        llegpos = body.transform.position.y - 1.1f;
        rlegpos = body.transform.position.y - 1.1f;
        larmpos = body.transform.position.y + 1.1f;
        rarmpos = body.transform.position.y + 1.1f;

        //left arm movement
        if (Input.GetKey(KeyCode.Q))
        {
            //LeftArmOut();
            BodyLeft();
            BodyUp();
        }
        else //if not holding, moves limb back to original position
        {
            //LeftArmIn();
        }

        //right arm movement
        if (Input.GetKey(KeyCode.P))
        {
            //RightArmOut();
            BodyRight();
            BodyUp();
        }
        else //if not holding, moves limb back to original position
        {
            //RightArmIn();
        }

        //left leg movement
        if (Input.GetKey(KeyCode.Z))
        {
            //LeftLegOut();
            BodyLeft();
            BodyDown();
        }
        else //if not holding, moves limb back to original position
        {
            //LeftLegIn();
        }

        //right leg movement
        if (Input.GetKey(KeyCode.M))
        {
            //RightLegOut();
            BodyRight();
            BodyDown();
        }
        else //if not holding, moves limb back to original position
        {
            //RightLegIn();
        }

        //move left
        if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.Z))
        {
            BodyLeft();
            //code to be sure diagonal movement doesn't happen
            if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.P))
            {
                upvel = 0;
                rarmvel = 0;
            }
            if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.M))
            {
                downvel = 0;
                rlegvel = 0;
            }
        }
        //move right
        if (Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.M))
        {
            BodyRight();
            //code to stop diagonal movement
            if (Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.M) && Input.GetKey(KeyCode.Q))
            {
                upvel = 0;
                larmvel = 0;
            }
            if (Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.M) && Input.GetKey(KeyCode.Z))
            {
                downvel = 0;
                llegvel = 0;
            }
        }
        //move up
        if (Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.Q))
        {
            BodyUp();
        }
        //move down
        if (Input.GetKey(KeyCode.M) && Input.GetKey(KeyCode.Z))
        {
            BodyDown();
        }
        
    }

    void BodyUp()
    {
        if (canvas.isActiveAndEnabled == false)
        {
            body.transform.Translate(Vector2.up * Time.deltaTime * upvel); //if both arms, move up

            if (body.transform.position.y >= 3) //if squirrel moves too close to edge of screen, stop movement
            {
                upvel = 0;
            }
            else upvel = 5;
        }
    }
    void BodyDown()
    {
        if (canvas.isActiveAndEnabled == false)
        {
            body.transform.Translate(Vector2.down * Time.deltaTime * downvel); //if both legs, move down

            if (body.transform.position.y <= -3) //if squirrel moves too close to edge of screen, stop movement
            {
                downvel = 0;
            }
            else downvel = 5;
        }
    }
    void BodyRight()
    {
        if (canvas.isActiveAndEnabled == false)
        {
            body.transform.Translate(Vector2.right * Time.deltaTime * rightvel); //if right tarm and right leg, move right

            if (body.transform.position.x >= 7) //if squirrel moves too close to edge of screen, stop movement
            {
                rightvel = 0;
            }
            else rightvel = 5;
        }
    }
    void BodyLeft()
    {
        if (canvas.isActiveAndEnabled == false)
        {
            body.transform.Translate(Vector2.left * Time.deltaTime * leftvel); //if left arm and left leg, move left

            if (body.transform.position.x <= -7) //if squirrel moves too close to the edge of the screen, stop movement
            {
                leftvel = 0;
            }
            else leftvel = 5;
        }
    }

    void LeftArmOut()
    {
        larm.transform.Translate(Vector2.left * Time.deltaTime * larmvel);
        larm.transform.Translate(Vector2.up * Time.deltaTime * larmvel);
        if (larm.transform.position.y >= larmpos + 0.5) //stops limb from moving too far by setting individual velocity to 0
        {
            larmvel = 0f;
        }
        else larmvel = mainVelocity; //speed that the limb moves out
    }
    void RightArmOut()
    {
        rarm.transform.Translate(Vector2.right * Time.deltaTime * rarmvel);
        rarm.transform.Translate(Vector2.up * Time.deltaTime * rarmvel);
        if (rarm.transform.position.y >= rarmpos + 0.5) //stops limb from moving too far by setting individual velocity to 0
        {
            rarmvel = 0f;
        }
        else rarmvel = mainVelocity; //speed that the limb moves out
    }
    void LeftLegOut()
    {
        lleg.transform.Translate(Vector2.left * Time.deltaTime * llegvel);
        lleg.transform.Translate(Vector2.down * Time.deltaTime * llegvel);
        if (lleg.transform.position.y <= llegpos - 0.5) //stops limb from moving too far by setting individual velocity to 0
        {
            llegvel = 0f;
        }
        else llegvel = mainVelocity; //speed that the limb moves out
    }
    void RightLegOut()
    {
        rleg.transform.Translate(Vector2.right * Time.deltaTime * rlegvel);
        rleg.transform.Translate(Vector2.down * Time.deltaTime * rlegvel);
        if (rleg.transform.position.y <= rlegpos - 0.5) //stops limb from moving too far by setting individual velocity to 0
        {
            rlegvel = 0f;
        }
        else rlegvel = mainVelocity; //speed that the limb moves out
    }
    void LeftArmIn()
    {
        larm.transform.Translate(Vector2.right * Time.deltaTime * larmvel);
        larm.transform.Translate(Vector2.down * Time.deltaTime * larmvel);
        if (larm.transform.position.y <= larmpos) //stops limb from moving too far by setting individual velocity to 0
        {
            larmvel = 0f;

        }
        else larmvel = mainVelocity; //speed that the limb moves in
    }
    void RightArmIn()
    {
        rarm.transform.Translate(Vector2.left * Time.deltaTime * rarmvel);
        rarm.transform.Translate(Vector2.down * Time.deltaTime * rarmvel);
        if (rarm.transform.position.y <= rarmpos) //stops limb from moving too far by setting individual velocity to 0
        {
            rarmvel = 0f;

        }
        else rarmvel = mainVelocity; //speed that the limb moves in
    }
    void LeftLegIn()
    {
        lleg.transform.Translate(Vector2.right * Time.deltaTime * llegvel);
        lleg.transform.Translate(Vector2.up * Time.deltaTime * llegvel);
        if (lleg.transform.position.y >= llegpos) //stops limb from moving too far by setting individual velocity to 0
        {
            llegvel = 0f;
        }
        else llegvel = mainVelocity; //speed that the limb moves in
    }
    void RightLegIn()
    {
        rleg.transform.Translate(Vector2.left * Time.deltaTime * rlegvel);
        rleg.transform.Translate(Vector2.up * Time.deltaTime * rlegvel);
        if (rleg.transform.position.y >= rlegpos) //stops limb from moving too far by setting individual velocity to 0
        {
            rlegvel = 0f;

        }
        else rlegvel = mainVelocity; //speed that the limb moves in
    }
}
