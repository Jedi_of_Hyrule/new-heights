using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartCounter : MonoBehaviour
{
    public TextMeshProUGUI counter;
    float number = 3f;
    public Canvas canvas;
    [SerializeField] SquirrelMovement squirrel;
    public bool start = false;

    private void Start()
    {
        squirrel.mainVelocity = 0f;
    }
    void Update()
    {
        counter.text = " " + Mathf.Round(number) + " ";
        number -= Time.deltaTime;
        if (number <= 0)
        {
            canvas.enabled = false;
            squirrel.mainVelocity = 1f;
            start = true;
        }
    }
}
