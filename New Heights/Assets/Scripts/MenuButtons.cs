using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Emily Testing");
    }

    public void Instructions()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void BackMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Replay()
    {
        SceneManager.LoadScene("Emily Testing");
    }

    public void LeaveGame()
    {
        Application.Quit();
    }
}
